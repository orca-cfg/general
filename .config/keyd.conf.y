[ids]
*

[main]
capslock = overload(capslock, esc)
102nd = layer(xalt)

[capslock:C]
a = left
s = down
w = up
d = right
q = home
e = end
r = pageup
f = pagedown

[xalt]
h = left
j = down
k = up
l = right
i = home
o = end
u = pageup
m = pagedown

# Activates when both capslock and shift is pressed
#[capslock+shift]
#a = C-left
#s = C-down
#w = C-up
#d = C-right

