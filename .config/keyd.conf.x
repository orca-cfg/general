[ids]
*

[main]
capslock = overload(capslock, esc)

[capslock:C]
a = left
s = down
w = up
d = right
q = home
e = end
r = pageup
f = pagedown

# Activates when both capslock and shift is pressed
[capslock+shift]
a = C-left
s = C-down
w = C-up
d = C-right

