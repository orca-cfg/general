#!/usr/bin/env zsh
#emulate -L zsh
source ~/.sexy.rc
sexy-dbg '|*#> .zshrc'

CASE_SENSITIVE="true"
DISABLE_LS_COLORS="true"

setopt equals
setopt prompt_subst
setopt transient_rprompt
setopt hist_expire_dups_first
setopt hist_save_no_dups
setopt hist_find_no_dups
setopt hist_ignore_all_dups
setopt hist_subst_pattern
setopt hist_no_store
setopt hist_reduce_blanks
setopt append_history
setopt extended_glob
setopt nullglob
setopt completeinword
setopt pushdignoredups
setopt globdots


unsetopt hist_ignore_space
unsetopt hist_no_functions

# setopt glob_subst
unset MAILCHECK
unset LS_COLORS

LISTMAX=0

typeset -gU path cdpath manpath fpath

sexy zsh .zshrc
sexy-dbg '|*#< .zshrc'


if [ -z $(starship --version  &> /dev/null) ]; then
    eval "$(starship init zsh)"
fi

if [ -z $(zoxide --version  &> /dev/null) ]; then
    eval "$(zoxide init zsh --cmd j --hook prompt)"
fi

if [ -z $(fzf --version  &> /dev/null) ]; then
    eval "$(fzf --zsh)"
fi

#export EDITOR="emacsclient -t -a ''"
#export VISUAL="emacsclient -c -a emacs"

export PATH="$PATH:/home/orca/Dev/SDK/Flutter/flutter/bin"
export PATH="$PATH:/home/orca/.emacs.d/bin"


[ -f "/home/orca/.ghcup/env" ] && . "/home/orca/.ghcup/env" # ghcup-env

