#!/usr/bin/env zsh
#emulate -L zsh
source ~/.sexy.rc
sexy-dbg '|*#> .zshenv'

typeset -gU PATH path cdpath manpath fpath

sexy zsh .zshenv
sexy-dbg '|*#< .zshenv'
