#!/usr/bin/env zsh
source ~/.sexy.rc
sexy-dbg '|*#> .profile'

export DOCKER_OPTS="--dns 8.8.8.8 --dns 8.8.4.4"

[ -f ~/.profile.local ] && source ~/.profile.local
test -z "$PROFILEREAD" && . /etc/profile || true

sexy zsh .profile

sexy-dbg '|*#<  .profile'

[ -f "/home/orca/.ghcup/env" ] && . "/home/orca/.ghcup/env" # ghcup-env