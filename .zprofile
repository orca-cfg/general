#!/usr/bin/env zsh
source ~/.sexy.rc
sexy-dbg '|*#> .zprofile'

[ ! `umask` -eq '022' ] && [ ! "$UID" -eq '0' ] && umask 022

# [ -f /etc/profile ] && source /etc/profile
[ -f ~/.profile ] && source ~/.profile

sexy zsh .zprofile

sexy-dbg '|*#< .zprofile'
